import axios from "axios";
import { useState, useEffect } from "react";

export default function CSR() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [params, setParams] = useState({
    _page: 1,
    _limit: 10,
  });

  let timeout;

  useEffect(() => {
    // setLoading = true;
    setData([])
    axios
        .get("https://jsonplaceholder.typicode.com/posts/", {
          params: {
            ...params,
          },
        })
        .then((response) => {
          setData([...response.data]);
          setLoading(false);
        })
        .catch((err) => {
          setData([]);
          setLoading(false);
        });
  }, [params]);
  return (
    <div className="w-full">
      <h1>Client Side</h1>
      <button
      className=""
      onClick={() => {
        setParams({...params, _page: params._page + 1})
      }}
      >
        {" "}
        Next Page{" "}
      </button>
      <div className="space-y-6 block text-black">
        {loading === true ? "Loading" : "SELESAI"}
        <pre>{JSON.stringify(data, null, 2)}</pre>
      </div>
    </div>
  );
}
