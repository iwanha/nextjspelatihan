import CardComponent from "@app/src/components/card.component";
import axios from "axios";

export default function Home(props) {
  let {id, title, thumbnail, description} = props
  console.log(props.data);
  return (
    <div className="h-screen w-full  bg-fixed mx-auto max-w-[900px]">
      <h1 className="text-primary">Home</h1>
      <CardComponent id={1} title="ini card" description="desc card component" thumbnail="/redditemblem.png">

      </CardComponent>
      
      {/* <div>
        {props.data.map((item) => {
          return (
            <div className="text-black bg-gray-100 rounded-xl mb-4 p-4">
              <p>title: {item.title}</p>
            </div>
          );
        })}
      </div> */}
    </div>
  );
}


export async function getServerSideProps(ctx) {
  let query = ctx.query;
  let params = {
    page: 1,
    limit: 2,
  };

  if (
    typeof query?.page !== "undefined" &&
    query?.page !== "" &&
    query?.page !== null
  ) {
    Reflect.set(params, "page", query?.page);
  }

  const response = await axios
    .get("https://jsonplaceholder.typicode.com/posts", {
      params: {
        ...params,
      },
    })
    .then((res) => {
      console.log({ data: res.data }, "RESPONSE API");
      return res.data;
    })
    .catch((err) => {
      console.log(err, "RESPONSE API");
      return [];
    });

  return {
    props: {
      data: response,
      // property key: data
    },
  };
}
