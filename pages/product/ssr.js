import CardComponent from "@app/src/components/card.component";
import axios from "axios";

/**
 *
 * @param {Object} props
 * @param {Object} props.pagination
 * @param {Number} props.pagination.limit
 * @param {Number} props.pagination.skip
 * @param {Number} props.pagination.total
 * @param {Array} props.data
 */
export default function SSR(props) {
  let { data, pagination } = props;

  return (
    <div className="text-black">
      {/* <pre>{JSON.stringify(props, null, 2)}</pre> */}
      <div className="pt-4 pb-4 pl-4 pr-4 grid grid-cols-4">
      {Array.isArray(data) && data.length > 0
        ? data.map((item) => {
            return (
                
                <CardComponent id={item.id} title={item.title} thumbnail={item.thumbnail} key={item}/>
                
        )})
            : "isEmpty"}
            </div>
    </div>
  );
}

export async function getServerSideProps() {
  const response = await axios
    .get(`http://localhost:3000/api/product`)
    .then((response) => {
      let data = response.data;
      console.log(data);
      return {
        pagination: {
          limit: data?.limit ?? 10,
          skip: data?.skip ?? 0,
          total: data?.total ?? 0,
        },
        data: data?.data ?? [],
      };
    })
    .catch((error) => {
      return {
        pagination: {
          limit: 10,
          skip: 0,
          total: 0,
        },
        data: [],
      };
    });
  return {
    props: {
      ...response,
    },
  };
}
