import axios from "axios";
import { useEffect, useState } from "react";

export default function Product() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [params, setParams] = useState({
    page: 1,
    limit: 10,
  });

  useEffect(()=> {
    setLoading(true);
    axios.get(
        "http://localhost:3000/api/product/"
    )
        .then((response)=> {
            console.log(response.data);
            if(
                response?.data?.data &&
                Array.isArray(response?.data?.data)
            ){
                setData([
                    ...response?.data?.data
                ])
            }

            setLoading(false);
        })
        .catch((err)=> {
            setData([]);
            setLoading(false);
        })
},[])
  return (
    <div className="w-full text-black">
      <h1>Product</h1>
      {loading ? (
        <p>Loading. . .</p>
      ) : Array.isArray(data) && data.length > 0 ? (
        data.map((item) => {
          return (
            <div className="text-black">
              <p>id: {item.id}</p>
              <p>title: {item.title}</p>
              <img src={item.thumbnail}>
              </img>
              {/* <pre>{JSON.stringify(item, null, 2)}</pre> */}
            </div>
          );
        })
      ) : (
        "isEmpty"
      )}
    </div>
  );
}
