import NavbarComponent from '@app/src/components/navbar.component';
import '@app/styles/globals.css'

import "@app/styles/sass/main.scss"
import {getSession, SessionProvider} from "next-auth/react";

export default function App(props) {
    let { Component, pageProps: {session,...pageProps} } = props
    return (
        <SessionProvider session={session}>
            <div
                className={
                    'app-main bg-fixed bg-white w-full min-h-screen'
                }>
                <NavbarComponent logo="/redditlogo.png"></NavbarComponent>
                <main className={'pt-20'}>
                    <Component {...pageProps} session={session}/>
                </main>

            </div>
        </SessionProvider>
    )
}


App.getInitialProps = async ({Component,ctx})=> {
    let pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    const session = await getSession(ctx);

    pageProps = {
        ...pageProps,
        session
    }
    return {
        pageProps
    }
}