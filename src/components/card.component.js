import { useState } from "react";
/**
 *
 * @param {Object} props
 * @param {String} props.thumbnail
 * @param {String} props.title
 * @param {String} props.description
 * @param {number} props.id
 * @returns
 */
export default function CardComponent(props) {
  let { id, title, thumbnail, description } = props;
  const [loading, setLoading] = useState(false);
  function addToCard() {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 2000);
    clearTimeout();
  }
  return (
    <div className="mb-2 mr-2 mt-2 ml-2">
      <section className="w-full min-w-[200px] bg-slate-400 pt-4 pb-4 pl-4 pr-4 rounded-xl app-card">
        {/* thumbnail */}
        <div className="app-card-img">
          <img
            src={thumbnail}
            alt="thumbnail-card"
            className="card-img__images"
          />
        </div>

        {/* children */}
        {props?.children}

        {/* content */}
        <div className="p-4 w-full text-primary">
          <h3>{title}</h3>
          <p>{description}</p>
          <div>
            <button onClick={addToCard}>
              {loading ? "Loading. . ." : "Add to Cart"}
            </button>
          </div>
        </div>
      </section>
    </div>
  );
}
