import Link from "next/link";

/**
 *
 * @param {Object} props
 * @param {String|null} props.logo
 * @returns
 */
export default function NavbarComponent(props) {
  let { logo } = props;
  return (
    <header className="w-full fixed top-0 h-20 bg bg-white">
      <div className="mx-auto max-w-[1280px]">
        <div>
          {logo !== null && (
            <img
              src={logo}
              alt="logo-page"
              className="w-20 object-contain"
            ></img>
          )}
          <nav>
            <ul className="flex items-center gap-4 text-warning">
              <li>
                {/* <a href="/">Home</a> */}
                <Link href={"/"}>Home</Link>
              </li>
              {/* <a href="/about">About</a> */}
              <Link href={"/about"}>About</Link>
              <li>
                  <Link href={"/product"}>ClientSideRender</Link>
                </li>
                <li>
                  <Link href={"/product/ssr"}> ServerSideRender</Link>
                </li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  );
}
